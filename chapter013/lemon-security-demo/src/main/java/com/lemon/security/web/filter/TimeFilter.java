package com.lemon.security.web.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author lemon
 * @date 2018/4/1 下午10:19
 */
public class TimeFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("time filter init.");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        System.out.println("time filter start.");
        long startTime = System.currentTimeMillis();
        chain.doFilter(request, response);
        System.out.println("time filter 耗时： " + (System.currentTimeMillis() - startTime));
        System.out.println("time filter finish.");
    }

    @Override
    public void destroy() {
        System.out.println("time filter destroy.");
    }
}
