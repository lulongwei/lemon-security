package com.lemon.security.web.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.lemon.security.web.dto.User;
import com.lemon.security.web.exception.UserNotExistException;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lemon
 * @date 2018/3/22 下午3:39
 */
@RestController
public class UserController {

    @RequestMapping(value = "/user1", method = RequestMethod.GET)
    public List<User> query1() {
        return generateUsers();
    }

    @GetMapping("/user2")
    public List<User> query2(@RequestParam String username) {
        System.out.println(username);
        return generateUsers();
    }

    @GetMapping("/user3/{username}")
    public List<User> query3(@PathVariable String username) {
        System.out.println(username);
        return generateUsers();
    }

    @GetMapping("/user4")
    public List<User> query4(@PageableDefault(page = 1, size = 2, sort = "username") Pageable pageable) {
        System.out.println(pageable.getPageNumber());
        System.out.println(pageable.getPageSize());
        System.out.println(pageable.getSort());
        return generateUsers();
    }

    @GetMapping("/getInfo/{id:\\d+}")
    public User getInfo(@PathVariable Integer id) {
        System.out.println("查询的对象ID为：".concat(String.valueOf(id)));
        User user = new User();
        user.setUsername("lemon");
        return user;
    }

    @GetMapping("/getSimpleUser")
    @JsonView(User.UserSimpleView.class)
    public User getSimpleUser() {
        User user = new User();
        user.setUsername("lemon");
        user.setPassword("123456");
        return user;
    }

    @GetMapping("/getDetailUser")
    @JsonView(User.UserDetailView.class)
    public User getDetailUser() {
        User user = new User();
        user.setUsername("lemon");
        user.setPassword("123456");
        return user;
    }

    @PostMapping("/user1")
    public User create1(@RequestBody User user) {
        System.out.println(ReflectionToStringBuilder.reflectionToString(user, ToStringStyle.MULTI_LINE_STYLE));
        user.setId(1);
        return user;
    }

    @PostMapping("/user2")
    public User create2(@Valid @RequestBody User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(error -> System.out.println(error.getDefaultMessage()));
        }
        System.out.println(ReflectionToStringBuilder.reflectionToString(user, ToStringStyle.MULTI_LINE_STYLE));
        user.setId(2);
        return user;
    }

    @PutMapping("/user/{id:\\d+}")
    public User update(@PathVariable Integer id) {
        User user = new User();
        user.setId(id);
        System.out.println("模拟修改");
        user.setUsername("lemon");
        return user;
    }

    @DeleteMapping("/user/{id:\\d+}")
    public void delete(@PathVariable Integer id) {
        System.out.println("模拟修改，修改ID：".concat(String.valueOf(id)));
    }

    @PostMapping("/user3")
    public User create3(@RequestBody @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(error -> System.out.println(error.getDefaultMessage()));
        }
        user.setId(3);
        return user;
    }

    @PostMapping("/user4")
    public User create4(@RequestBody @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(error -> System.out.println(error.getDefaultMessage()));
        }
        user.setId(4);
        return user;
    }

    @GetMapping("/user5")
    @ResponseBody
    public User user5() {
        throw new RuntimeException("User is not exist.");
    }

    @GetMapping("/user6/{id:\\d+}")
    @ResponseBody
    public User user6(@PathVariable Integer id) {
        throw new UserNotExistException(id);
    }

    @PostMapping("/user7/{id:\\d+}")
    @ResponseBody
    @JsonView(User.UserDetailView.class)
    public User user7(@PathVariable Integer id) {
        User user = new User();
        user.setId(id);
        user.setUsername("tom");
        user.setPassword("123456");
        user.setBirthday(new Date());
        return user;
    }

    private List<User> generateUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new User());
        users.add(new User());
        return users;
    }
}
