package com.lemon.security.web.dto;

import lombok.Data;

/**
 * @author lemon
 * @date 2018/4/2 下午2:24
 */
@Data
public class FileInfo {

    private String path;

    public FileInfo(String path) {
        this.path = path;
    }
}
