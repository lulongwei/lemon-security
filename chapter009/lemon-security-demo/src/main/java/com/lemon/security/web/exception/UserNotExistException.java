package com.lemon.security.web.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author lemon
 * @date 2018/4/1 下午2:08
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserNotExistException extends RuntimeException {

    private Integer id;

    public UserNotExistException(Integer id) {
        super("user not exist.");
        this.id = id;
    }
}
