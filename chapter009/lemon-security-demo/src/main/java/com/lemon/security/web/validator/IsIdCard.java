package com.lemon.security.web.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义身份证号码校验注解
 *
 * @author lemon
 * @date 2018/3/31 下午7:43
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IdCardValidator.class)
public @interface IsIdCard {

    String message();

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}
