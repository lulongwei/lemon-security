package com.lemon.security.core.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * @author jiangpingping
 * @date 2019-02-05 17:59
 */
@Getter
@Setter
public class SocialProperties {

    /**
     * 这个属性是为了设置自定义社交登录拦截路径的
     */
    private String filterProcessesUrl = "/auth";

    private QQProperties qq = new QQProperties();

}
