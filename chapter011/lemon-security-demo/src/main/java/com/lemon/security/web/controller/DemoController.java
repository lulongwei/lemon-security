package com.lemon.security.web.controller;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * @author lemon
 * @date 2018/3/18 下午5:46
 */
@Controller
public class DemoController {

    @GetMapping("/hello")
    @ResponseBody
    public String hello() {
        Resource resource = new ClassPathResource("resources/test.txt");
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Hello Spring Security";
    }
}
