package com.lemon.security.web.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lemon
 * @date 2018/4/2 下午2:24
 */
@Data
public class FileInfo {

    @ApiModelProperty(value = "文件上传后的文件路径")
    private String path;

    public FileInfo(String path) {
        this.path = path;
    }
}
