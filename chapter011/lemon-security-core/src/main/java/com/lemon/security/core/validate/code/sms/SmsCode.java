package com.lemon.security.core.validate.code.sms;

import com.lemon.security.core.validate.code.ValidateCode;

import java.time.LocalDateTime;

/**
 * 短信验证码实体类
 *
 * @author lemon
 * @date 2018/4/17 下午8:18
 */
public class SmsCode extends ValidateCode {

    public SmsCode(String code, LocalDateTime expireTime) {
        super(code, expireTime);
    }

    public SmsCode(String code, int expireIn) {
        super(code, LocalDateTime.now().plusSeconds(expireIn));
    }
}
