package com.lemon.security.browser.support;

import lombok.Data;

/**
 * @author lemon
 * @date 2018/4/5 下午2:33
 */
@Data
public class SimpleResponse {

    private Object content;

    public SimpleResponse(String content) {
        this.content = content;
    }
}
