package com.lemon.security.web.service.impl;

import cn.hutool.core.util.IdcardUtil;
import com.lemon.security.web.service.IdCardValidatorService;
import org.springframework.stereotype.Service;

/**
 * @author lemon
 * @date 2018/3/31 下午8:14
 */
@Service
public class IdCardValidatorServiceImpl implements IdCardValidatorService {

    @Override
    public boolean valid(String value) {
        return IdcardUtil.isValidCard(value);
    }
}
