package com.lemon.security.core.validate.code.sms;

/**
 * 短信验证发送接口
 *
 * @author lemon
 * @date 2018/4/17 下午8:25
 */
public interface SmsCodeSender {

    /**
     * 短信验证码发送接口
     *
     * @param mobile 手机号
     * @param code   验证码
     */
    void send(String mobile, String code);
}
