package com.lemon.security.core.properties;

import lombok.Data;

/**
 * 封装多个配置的类
 *
 * @author lemon
 * @date 2018/4/6 下午9:45
 */
@Data
public class ValidateCodeProperties {

    private ImageCodeProperties image = new ImageCodeProperties();
}
