package com.lemon.security.core.properties;

import lombok.Getter;

/**
 * @author lemon
 * @date 2018/4/5 下午8:06
 */
@Getter
public enum LoginType {

    /**
     * 跳转
     */
    REDIRECT,

    /**
     * 返回JSON
     */
    JSON
}
