package com.lemon.security.web.controller;

import com.lemon.security.web.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户注册的Controller
 *
 * @author jiangpingping
 * @date 2019-02-18 19:43
 */
@RestController
@RequestMapping("/demo")
public class RegisterController {

    private final ProviderSignInUtils providerSignInUtils;

    @Autowired
    public RegisterController(ProviderSignInUtils providerSignInUtils) {
        this.providerSignInUtils = providerSignInUtils;
    }

    @PostMapping(value = "/register")
    public String register(User user, HttpServletRequest request) {
        // 不管是注册还是绑定，都会拿到用户在业务系统中的唯一标识，注册是新生成标识，绑定是从数据库中获取唯一标识
        // 那么我们就以用户传递过来名称作为唯一标识，将这个标识和session中的用户信息一同传输给Spring Social
        // Spring Social拿到数据以后，就会将这个唯一标识和用户在QQ上的信息一同存储到UserConnection表中
        String userId = user.getUsername();
        providerSignInUtils.doPostSignUp(userId, new ServletWebRequest(request));
        return "注册并绑定成功";
    }

}
