package com.lemon.security.web.authentication;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Component;

/**
 * 默认为用户注册账户的实现类
 *
 * @author jiangpingping
 * @date 2019-02-20 20:20
 */
@Component
public class DemoConnectionSignUp implements ConnectionSignUp {

    @Override
    public String execute(Connection<?> connection) {
        // 这里应该写与业务相关的默认注册行为，这里为了简便，生成的系统用户的userId就是要QQ的相关信息
        // 这里使用的是QQ用户对本网站的唯一的openId作为userId来注册的
        return connection.getKey().getProviderUserId();
    }

}
