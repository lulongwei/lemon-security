package com.lemon.security.web.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author lemon
 * @date 2018/3/18 下午5:44
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.lemon.security"})
public class MainApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }
}
