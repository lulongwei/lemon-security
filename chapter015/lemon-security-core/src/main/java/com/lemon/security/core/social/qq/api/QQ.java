package com.lemon.security.core.social.qq.api;

/**
 * 获取QQ用户信息的接口
 *
 * @author jiangpingping
 * @date 2019-02-05 11:30
 */
public interface QQ {

    /**
     * 获取QQ用户的信息
     *
     * @return QQ用户信息
     */
    QQUserInfo getUserInfo();

}
