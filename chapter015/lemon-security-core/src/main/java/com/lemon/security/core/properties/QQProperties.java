package com.lemon.security.core.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.social.SocialProperties;

/**
 * @author jiangpingping
 * @date 2019-02-05 17:56
 */
@Getter
@Setter
public class QQProperties extends SocialProperties {

    private String providerId = "qq";

}
