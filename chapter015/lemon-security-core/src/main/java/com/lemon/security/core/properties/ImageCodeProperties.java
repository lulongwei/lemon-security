package com.lemon.security.core.properties;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图形验证码的默认配置
 *
 * @author lemon
 * @date 2018/4/6 下午9:42
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ImageCodeProperties extends CodeProperties {

    public ImageCodeProperties() {
        setLength(4);
    }

    /**
     * 验证码宽度
     */
    private int width = 67;
    /**
     * 验证码高度
     */
    private int height = 23;
}
