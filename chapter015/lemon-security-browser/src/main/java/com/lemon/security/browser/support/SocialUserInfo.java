package com.lemon.security.browser.support;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jiangpingping
 * @date 2019-02-18 20:49
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class SocialUserInfo {

    /**
     * 应用标识，比如qq，wechat，facebook
     */
    private String providerId;

    /**
     * 用户的openId
     */
    private String providerUserId;

    /**
     * 用户在第三方的昵称
     */
    private String nickname;

    /**
     * 用户在第三方的头像地址
     */
    private String headImg;

}
