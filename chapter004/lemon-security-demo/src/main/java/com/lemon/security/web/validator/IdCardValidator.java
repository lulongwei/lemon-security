package com.lemon.security.web.validator;

import com.lemon.security.web.service.IdCardValidatorService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 校验注解的校验逻辑
 *
 * @author lemon
 * @date 2018/3/31 下午7:57
 */
public class IdCardValidator implements ConstraintValidator<IsIdCard, String> {

    @Autowired
    private IdCardValidatorService idCardValidatorService;

    /**
     * 校验前的初始化工作
     *
     * @param constraintAnnotation 自定义的校验注解
     */
    @Override
    public void initialize(IsIdCard constraintAnnotation) {
        String message = constraintAnnotation.message();
        System.out.println("用户自定义的message信息是：".concat(message));
    }

    /**
     * 具体的校验逻辑方法
     *
     * @param value   需要校验的值，从前端传递过来
     * @param context 校验器的校验环境
     * @return 通过校验返回true，否则返回false
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return idCardValidatorService.valid(value);
    }
}
